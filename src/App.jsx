import React, { useState, useEffect } from 'react';
import './App.css';
import Header from './components/Header';
import BoardsList from "./components/BoardsList"; 
import BoardView from './components/Boardview';
import ErrorPage from '../src/Pages/ErrorPage'; 
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom'; 
import { getAllBoards, createBoard } from '../src/Api'; 
import CircularLoading from './Pages/LoadingPage'; 

export const AppContext = React.createContext();

function App() {
  const [boards, setBoards] = useState([]);
  const [loading, setLoading] = useState(true); 
  const [error, setError] = useState(null); 

  useEffect(() => {
    fetchBoards();
  }, []);

  const fetchBoards = async () => {
    try {
      const response = await getAllBoards(); 
      setBoards(response.data || []); 
      setLoading(false); 
    } catch (error) {
      setError(error); 
      setLoading(false);  
    }
  };

  const handleCreateBoard = async (boardName) => {
    try {
      await createBoard(boardName); 
      fetchBoards(); 
    } catch (error) {
       setError(error); 
    }
  };

  if (loading) {
    return <CircularLoading />;
  }

  return (
    <body style={{background:"rgb(33,46,55)",height:"100vh"}}>
      <Router>
        <div>
          <Header />
          <AppContext.Provider value={{ boards, createBoard: handleCreateBoard }}> 
              <Routes>
                <Route path="/" element={<Navigate to="/boards" replace />} />
                <Route exact path="/boards" element={<BoardsList />} />  
                <Route path="/boards/:board_id" element={<BoardView />} />
                <Route path="*" element={<ErrorPage />} /> 
              </Routes>
          </AppContext.Provider>
        </div>
      </Router>
    </body>
  );
}

export default App;
