import React, { useContext, useState } from 'react'; 
import { AppContext } from '../App';
import AddForm from './addform'; 
import ErrorPage from '../Pages/ErrorPage'; 
import "../App.css";

function CreateBoard() {
  const { createBoard } = useContext(AppContext);
  const [showForm, setShowForm] = useState(false); 
  const [error, setError] = useState(null); 

  const handleCreateBoard = async (boardName) => {
    if (boardName.trim() !== '') {
      try {
        await createBoard(boardName);
        setLoading(false); 
        setShowForm(false); 
      } catch (error) {
        setError(error);
      }
    } else {
      setError('Please enter a board name'); y
    }
  };

  const handleClick = () => {
    setShowForm(true); 
  };

  
  return (
    <>
      {!showForm && (
        <span className="span" onClick={handleClick}>
          Create Board
        </span>
      )}
      {showForm && (
        <AddForm
          placeholder="Enter board name"
          handleCreateList={handleCreateBoard}
          setShowForm={setShowForm}
        />
      )}
    </>
  );
}

export default CreateBoard;
