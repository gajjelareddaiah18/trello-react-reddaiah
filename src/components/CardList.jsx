
import React, { useState, useEffect } from 'react';
import { Delete, CheckBox } from '@mui/icons-material';
import { Dialog, DialogContent, DialogTitle } from '@mui/material';
import LinearWithValueLabel from './LinearWithValueLabel';
import { fetchChecklists } from '../Api';
import { handleAddCheckList, 
          handleAddChecklistItem, 
          handleDeleteChecklistItem, 
          handleDeleteChecklist, 
          handleToggleCheckItem,
          useFetchChecklists  } from './checklistFunctions';

const CardList = ({ cards, handleCardDelete }) => {
  const [deleteConfirmation, setDeleteConfirmation] = useState(null);
  const [checklistOpen, setChecklistOpen] = useState(false);
  const [checklists, setChecklists] = useState([]);
  const [selectedCardId, setSelectedCardId] = useState(null);
  const [newCheckListName, setNewCheckListName] = useState('');
  const [newItemName, setNewItemName] = useState('');



  // events
  const openDeleteConfirmation = (cardId) => {
    setSelectedCardId(cardId);
    setDeleteConfirmation(true);
  };

  const closeDeleteConfirmation = () => {
    setSelectedCardId(null);
    setDeleteConfirmation(false);
  };

  const openChecklistPopup = (cardId) => {
    setSelectedCardId(cardId);
    setChecklistOpen(true);
    fetchChecklists(cardId);
  };

  const closeChecklistPopup = () => {
    setChecklistOpen(false);
  };

  const handleInputChange = (event) => {
    setNewCheckListName(event.target.value);
  };

  const handleItemInputChange = (event) => {
    setNewItemName(event.target.value);
  };

  //functions imported


  useFetchChecklists(selectedCardId, setChecklists);

  const handleAddCheckListWrapper = async (event) => {
    event.preventDefault();
    await handleAddCheckList(selectedCardId, newCheckListName, setNewCheckListName, setChecklists, closeChecklistPopup);
  };

  const handleAddChecklistItemWrapper  = async (checklistId) => {
    try {
      await handleAddChecklistItem(checklistId, newItemName, setChecklists, selectedCardId);
      setNewItemName('');
    } catch (error) {
      console.error('Error adding checklist item:', error);
    }
  };

  const handleDeleteChecklistItemWrapper = async (checklistId, itemId) => {
    try {
      await handleDeleteChecklistItem(checklistId, itemId, setChecklists, selectedCardId);
    } catch (error) {
      console.error('Error deleting checklist item:', error);
    }
  };

  const handleDeleteChecklistWrapper = async (checklistId) => {
    try {
      await handleDeleteChecklist(checklistId, setChecklists, selectedCardId);
    } catch (error) {
      console.error('Error deleting checklist:', error);
    }
  };

  const handleToggleCheckItemWrapper = async (checklistId, itemId, currentState) => {
    try {
      await handleToggleCheckItem(checklistId, itemId, currentState, setChecklists, selectedCardId);
    } catch (error) {
      console.error('Error toggling check item state:', error);
    }
  };

  const calculateProgress = (checklist) => {
    if (checklist.checkItems.length === 0) return 0;

    const completedItems = checklist.checkItems.filter((item) => item.state === 'complete').length;
    return (completedItems / checklist.checkItems.length) * 100;
  };

  return (
    <div>
      <ul>
        {cards &&
          cards.map((card) => (
            <li
              key={card.id}
              className="cards"
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                padding: '0.5rem',
                marginTop: 10,
                boxShadow:
                  'rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px',
              }}
              onClick={() => openDeleteConfirmation(card.id)}
            >
              {card.name}
              <Delete
                onClick={(e) => {
                  e.stopPropagation();
                  handleCardDelete(card.id);
                }}
              />
            </li>
          ))}
      </ul>
      <Dialog open={!!deleteConfirmation} onClose={closeDeleteConfirmation}>
        <DialogContent style={{ width: 600, height: 'auto', background: 'rgb(43,55,67)' }}>
          <button
            onClick={() => openChecklistPopup(selectedCardId)}
            style={{ marginTop: 20, marginLeft: 450, display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}
          >
            <CheckBox /> Checklist
          </button>
          {checklists && checklists.length > 0 && (
            <ul>
              {checklists.map((checklist) => (
                <li key={checklist.id} style={{ width: 400, height: 'auto', marginTop: 20, borderRadius: 5, color: 'blue', fontSize: 30 }}>
                  <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <p>{checklist.name}</p>
                    <Delete onClick={() => handleDeleteChecklistWrapper(checklist.id)} />
                  </div>
                  <LinearWithValueLabel progress={calculateProgress(checklist)} />
                  <ul>
                    {checklist.checkItems &&
                      checklist.checkItems.map((checkItem) => (
                        <li key={checkItem.id} style={{ display: 'flex', alignItems: 'center', marginTop: 5 ,width:300,justifyContent:"space-between",boxShadow:"rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px",color:"white",fontSize:20 }}>
                          <input
                            type="checkbox"
                            checked={checkItem.state === 'complete'}
                            onChange={() => handleToggleCheckItemWrapper(checklist.id, checkItem.id, checkItem.state)}
                          />
                          <span style={{ textDecoration: checkItem.state === 'complete' ? 'line-through' : 'none', marginLeft: 5 }}>
                            {checkItem.name}
                          </span>
                          <button onClick={() => handleDeleteChecklistItemWrapper(checklist.id, checkItem.id)}>Del</button>
                        </li>
                      ))}
                    <li style={{ marginTop: 5, borderRadius: 15 }}>
                      <form onSubmit={(e) => { e.preventDefault(); handleAddChecklistItemWrapper(checklist.id); }}>
                        <input type="text" value={newItemName} onChange={handleItemInputChange} placeholder="Add new item" />
                      </form>
                    </li>
                  </ul>
                </li>
              ))}
            </ul>
          )}
        </DialogContent>
      </Dialog>
      <Dialog style={{ marginLeft: 500, marginTop: 120 }} open={checklistOpen} onClose={closeChecklistPopup}>
        <DialogContent style={{ width: 230, height: 170, marginTop: -10 }}>
          <DialogTitle style={{ textAlign: 'center', marginTop: -30 }}>Add Checklist</DialogTitle>
          <form onSubmit={handleAddCheckListWrapper}>
            <p>Title</p>
            <input type="text" value={newCheckListName} onChange={handleInputChange} />
            <button type="submit">Add</button>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default CardList;
