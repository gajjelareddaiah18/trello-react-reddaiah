import { useEffect } from 'react';
import { fetchChecklists,addChecklist, addChecklistItem, deleteChecklistItem, deleteChecklist, toggleCheckItem } from '../Api';

// fetch the lists

export const useFetchChecklists = (selectedCardId, setChecklists) => {
    useEffect(() => {
      const fetchingList = async () => {
        if (selectedCardId !== null) {
          const response = await fetchChecklists(selectedCardId);
          setChecklists(response);
        }
      };
      fetchingList();
    }, [selectedCardId, setChecklists]);
  };


//add checklist function
export const handleAddCheckList = async (selectedCardId, newCheckListName, setNewCheckListName, setChecklists, closeChecklistPopup) => {
  try {
    const res = await addChecklist(selectedCardId, newCheckListName);
    setNewCheckListName('');
    setChecklists(prevChecklists => [...prevChecklists, res]);
    closeChecklistPopup();
  } catch (error) {
    console.error('Error adding checklist:', error);
  }
};

//add checkItem function
export const handleAddChecklistItem = async (checklistId, newItemName, setChecklists, selectedCardId) => {
  try {
    const newItem = await addChecklistItem(checklistId, newItemName);
    setChecklists((prevChecklists) =>
      prevChecklists.map((checklist) =>
        checklist.id === checklistId
          ? { ...checklist, checkItems: [...checklist.checkItems, newItem] }
          : checklist
      )
    );
  } catch (error) {
    console.error('Error adding checklist item:', error);
  }
};

// delete ChecklistItem  function
export const handleDeleteChecklistItem = async (checklistId, itemId, setChecklists, selectedCardId) => {
  try {
    await deleteChecklistItem(checklistId, itemId);
    const updatedChecklists = await fetchChecklists(selectedCardId);
    setChecklists(updatedChecklists);
  } catch (error) {
    console.error('Error deleting checklist item:', error);
  }
};

// deleate checklist function
export const handleDeleteChecklist = async (checklistId, setChecklists, selectedCardId) => {
  try {
    await deleteChecklist(checklistId);
    const updatedChecklists = await fetchChecklists(selectedCardId);
    setChecklists(updatedChecklists);
  } catch (error) {
    console.error('Error deleting checklist:', error);
  }
};

// checkbox function

export const handleToggleCheckItem = async (checklistId, itemId, currentState, setChecklists, selectedCardId) => {
  try {
    await toggleCheckItem(selectedCardId, checklistId, itemId, currentState);
    const updatedChecklists = await fetchChecklists(selectedCardId);
    setChecklists(updatedChecklists);
  } catch (error) {
    console.error('Error toggling check item state:', error);
  }
};


