import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Typography } from '@mui/material'; 
import "../App.css";
import { AppContext } from "../App";
import CreateBoard from './creteboard';
import ErrorComponent from '../Pages/erroralert';
import CircularLoading from '../Pages/LoadingPage'; 


function BoardsList() {
  const { boards, error } = useContext(AppContext);
  const [loading, setLoading] = useState(true);
  const [showError, setShowError] = useState(false);

  useEffect(() => {
    setLoading(true);
    const delay = setTimeout(() => {
      setLoading(false);
    }, 1500);

    return () => clearTimeout(delay);
  }, []);


  if (loading) {
    return <CircularLoading />;
  }

  if (showError) {
    return <ErrorComponent open={true} onClose={() => setShowError(false)} message={error} />;
  }

  
  return (
    <div className='boards'>
      <ul className='board-list' style={{ display: 'flex', flexWrap: "wrap"}}>
        {(!boards || boards.length === 0) && <ErrorComponent open={true} onClose={() => setShowError(false)} message="No boards available" />}
        <CreateBoard />
        {boards.map((board) => (
          <li key={board.id} style={{ margin: 10 }}>
            <Link to={`/boards/${board.id}`} style={{ textDecoration: 'none' }}>
              <div className='board-item' style={{ background: "rgb(43,55,67)"}}>
                <Typography variant="body2" color="white">
                  {board.name}
                </Typography>
              </div>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default BoardsList;