import React, { useState, useEffect, createContext } from 'react';
import { Delete } from '@mui/icons-material';
import { useParams } from 'react-router-dom';
import CreateList from "./creteList"; 
import CreateCard from './createcard';
import CardList from './CardList';
import ErrorComponent from '../Pages/erroralert'; 
import CircularLoading from '../Pages/LoadingPage';

import { getAllLists, allcreateList, alldeleteLists, allcreateCards, alldeleteCards} from '../Api';

export const BoardViewContext = createContext();

function BoardView() {
  const { board_id } = useParams();
  const [board, setBoard] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchBoard = async () => {
      try {
        setTimeout(async () => {
          const lists = await getAllLists(board_id);
          if (Array.isArray(lists)) {
            setBoard(lists);
          } else {
            throw new Error('Invalid data format for lists');
          }
        }, 1000);
      } catch (error) {
        setError(error.message || 'An error occurred while fetching the board.');
      } finally {
        setTimeout(() => setLoading(false), 1000);
      }
    };
    fetchBoard();
  }, []);

  if (loading) {
    return <CircularLoading />;
  }

  const handleCreateList = async (newListName) => {
    try {
      const newList = await allcreateList(board_id, newListName);
      setBoard(prevBoard => [...prevBoard, newList]);
    } catch (error) {
      setError(error.message || 'An error occurred while creating the list.');
    }
  };

  const handleDeleteList = async (listId) => {
    try {
      await alldeleteLists(listId);
      setBoard(prevBoard => prevBoard.filter(list => list.id !== listId));
    } catch (error) {
      setError(error.message || 'An error occurred while deleting the list.');
    }
  };

  const handleCardCreate = async (cardName, listId) => {
    try {
      const newCard = await allcreateCards(cardName, listId);
      const updatedBoard = board.map(list => {
        if (list.id === listId) {
          return {
            ...list,
            cards: [...list.cards, newCard]
          };
        }
        return list;
      });
      setBoard(updatedBoard);
    } catch (error) {
      setError(error.message || 'An error occurred while creating the card.');
    }
  };

  const handleCardDelete = async (cardId) => {
    try {
      await alldeleteCards(cardId);
      const updatedBoard = board.map(list => ({
        ...list,
        cards: list.cards.filter(card => card.id !== cardId)
      }));
      setBoard(updatedBoard);
    } catch (error) {
      setError(error.message || 'An error occurred while deleting the card.');
    }
  };

  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <div className='boards'>
      <ul className='board-list' style={{ display: 'flex', flexWrap: 'wrap' }}>
        {Array.isArray(board) && board.map(list => (
          <li key={list.id} style={{ margin: 10, width: 300, height: 'auto', backgroundColor: "rgb(241,242,244)", marginTop: 40, borderRadius: 10, display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
            <div>
              <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '0.5rem',color:"blue" ,fontSize:"30px"}}>
                <span>{list.name}</span>
                <Delete onClick={() => handleDeleteList(list.id)} />
              </div>
             
              {list.cards && list.cards.length === 0 ? (
                <ErrorComponent open={true} onClose={() => {}} message="No cards available" />
              ) : (
                <CardList cards={list.cards} handleCardDelete={handleCardDelete} />
              )}
              
              <BoardViewContext.Provider key={list.id} value={{ listId: list.id, onCardCreate: handleCardCreate }}>
                <CreateCard  />
              </BoardViewContext.Provider>
            </div>
          </li>
        ))}
        {(Array.isArray(board) && board.length === 0) && <ErrorComponent open={true} onClose={() => {}} message="No lists available" />}
        <BoardViewContext.Provider value={{ handleCreateList }}>
          <CreateList />
        </BoardViewContext.Provider>
      </ul>
    </div>
  );
  
 }
export default BoardView;
