import React, { useState, useContext } from 'react';
import { BoardViewContext } from './Boardview';
import Button from '@mui/joy/Button';
import AddForm from './addform'; 

function CreateCard() {
  const { listId, onCardCreate } = useContext(BoardViewContext);
  const [showInput, setShowInput] = useState(false);
  const [error, setError] = useState(null);

  const handleCreateCard = async (newCardName) => {
    try {
      if (newCardName.trim() === '') {
        throw new Error('Card name cannot be empty');
      }
      await onCardCreate(newCardName, listId);
      setShowInput(false);
      setError(null);
    } catch (error) {
      setError(error.message || 'An error occurred while creating the card.');
    }
  };

  const handleButtonClick = () => {
    setShowInput(true);
  };

  const handleCancel = () => {
    setShowInput(false);
  };

  return (
    <div>
      {!showInput && (
        <Button
          style={{ width: '100%', marginTop: 10, marginBottom: 20, height: 30 }}
          onClick={handleButtonClick}
        >
          Create New Card
        </Button>
      )}
      {showInput && (
        <AddForm
          placeholder="Enter card name"
          handleCreateList={handleCreateCard}
          setShowForm={setShowInput}
        />
      )}
      {error && <div style={{ color: 'red' }}>{error}</div>}
    </div>
  );
}

export default CreateCard;
