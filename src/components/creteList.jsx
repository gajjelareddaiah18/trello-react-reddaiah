import React, { useState, useContext } from 'react';
import { BoardViewContext } from './Boardview'; 
import AddIcon from '@mui/icons-material/Add';
import AddForm from './addform'; 
import "../App.css"
export default function CreateList({ placeholder }) {
  const { handleCreateList } = useContext(BoardViewContext);
  const [showForm, setShowForm] = useState(false); 

  const handleClick = () => {
    setShowForm(true); 
  };

  return (
    <>
      {!showForm && (
        <span className="create-list-button" onClick={handleClick}>
          <AddIcon />
          Create List
        </span>
      )}
      {showForm && (
        <AddForm
          placeholder={placeholder || "Enter list name"} 
          setShowForm={setShowForm} 
          handleCreateList={handleCreateList} 
        />
      )}
    </>
  );
}
