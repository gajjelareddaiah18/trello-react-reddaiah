import React, { useState } from 'react';
import Textarea from '@mui/joy/Textarea';
import Button from '@mui/joy/Button';
import CloseIcon from '@mui/icons-material/Close';
import "../App.css"

const AddForm = ({ placeholder, setShowForm, handleCreateList }) => {
  const [newListName, setNewListName] = useState('');

  const handleSubmit = () => {
    if (newListName.trim() !== '') {
      handleCreateList(newListName)
        .then(() => {
          console.log('New list added:', newListName); 
          setNewListName('');
          setShowForm(false);
        })
        .catch(error => {
          console.error('Error adding new list:', error);
        });
    } else {
      console.log("Error: newListName is empty");
    }
  };

  const handleCancel = () => {
    setShowForm(false); 
  };

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        handleSubmit();
      }}
      className="listadder"
    >
      <Textarea
        required
        size="sm"
        name="Size"
        placeholder={placeholder}
        autoFocus
        onChange={(e) => setNewListName(e.target.value)} 
        value={newListName} 
      />
      <div className="addbutton">
        <Button type="submit" size="sm">
          Add
        </Button>

        <CloseIcon
          size="sm"
          className="closeIcon"
          onClick={handleCancel}
        />
      </div>
    </form>
  );
};

export default AddForm;