import axios from "axios";

export const apiKey = 'bf0212f6f07630c11427ce957892d38f';
export const token = 'ATTA13882f2f86cbefe77d0fca14133b28bc6c4e60bf48b740dda4161a56365aab3e339D7BAE';
export const baseUrl = "https://api.trello.com/1/";

axios.defaults.params = {
  key: apiKey,
  token: token,
};

//Board API
export const getAllBoards = async () => {
  const response = await axios.get(`${baseUrl}members/me/boards`);
  return response;
};

export const createBoard = async (boardName) => {
  const response = await axios.post(`${baseUrl}boards`, { name: boardName });
  return response;
};

//List API
export const getAllLists = async (board_id) => {
  try {
    const response = await axios.get(`${baseUrl}boards/${board_id}/lists?cards=all`);
    return response.data;
  } catch (error) {
    throw new Error('Failed to fetch lists from Trello API');
  }
};

export const allcreateList = async (board_id, newListName) => {
  const response = await axios.post(`${baseUrl}lists`, { name: newListName, idBoard: board_id });
  return response.data;
};

export const alldeleteLists = async (listId) => {
  await axios.put(`${baseUrl}lists/${listId}/closed`, { value: true });
};

// Card API
export const alldeleteCards = async (cardId) => {
  await axios.delete(`${baseUrl}cards/${cardId}`);
};

export const allcreateCards = async (cardName, listId) => {
  try {
    const response = await axios.post(`${baseUrl}cards`, { name: cardName, idList: listId });
    return response.data;
  } catch (error) {
    throw new Error('An error occurred while creating the card.');
  }
};

// Checklists

export const fetchChecklists = async (cardId) => {
  try {
    const response = await axios.get(`${baseUrl}cards/${cardId}/checklists?checkItems=all`);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const addChecklist = async (selectedCardId, newCheckListName) => {
  const response = await axios.post(`${baseUrl}cards/${selectedCardId}/checklists`, { name: newCheckListName });
  return response.data;
};

export const deleteChecklist = async (checklistId) => {
  const response = await axios.delete(`${baseUrl}checklists/${checklistId}`);
  return response.data;
};

// CheckItems

export const addChecklistItem = async (checklistId, newItemName) => {
  const response = await axios.post(`${baseUrl}checklists/${checklistId}/checkItems`, { name: newItemName });
  return response.data;
};

export const deleteChecklistItem = async (checklistId, itemId) => {
  const response = await axios.delete(`${baseUrl}checklists/${checklistId}/checkItems/${itemId}`);
  return response.data;
};

// Toggle check item

export const toggleCheckItem = async (selectedCardId, checklistId, itemId, currentState) => {
  try {
    const newState = currentState === 'complete' ? 'incomplete' : 'complete';
    await axios.put(`${baseUrl}cards/${selectedCardId}/checkItem/${itemId}`, { state: newState });
  } catch (error) {
    console.error('Error toggling check item:', error);
    throw new Error('Failed to toggle check item state. Please ensure that the card and check item exist.');
  }
};
